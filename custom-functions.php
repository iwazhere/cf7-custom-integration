<?php
/*
    Plugin Name: Custom Functions DL
*   Plugin URI: https://depthlogistics.com
*   description: All custom functions placed here
*   Version: 1.0
*   Author: Mikky
*   Author URI: https://depthlogistics.com
*   License: GPL2
*
*  All custom codes starts here
*/


function getUserIP()
{
$client  = @$_SERVER['HTTP_CLIENT_IP'];
$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
$remote  = $_SERVER['REMOTE_ADDR'];
if(filter_var($client, FILTER_VALIDATE_IP))
{
$ip = $client;
}
elseif(filter_var($forward, FILTER_VALIDATE_IP))
{
$ip = $forward;
}
else
{
$ip = $remote;
}
return $ip;
}
function get_client_ip() {
$ipaddress = '';
if (isset($_SERVER['HTTP_CLIENT_IP']))
$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_X_FORWARDED']))
$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_FORWARDED']))
$ipaddress = $_SERVER['HTTP_FORWARDED'];
else if(isset($_SERVER['REMOTE_ADDR']))
$ipaddress = $_SERVER['REMOTE_ADDR'];
else
$ipaddress = 'UNKNOWN';
return $ipaddress;
}
/* Querying jobs post title to the contact form 7 values on Single Job page*/

function custom_views_post_title_on_single_page() {
wpcf7_add_shortcode( 'custom_views_post_title_on_single_page', 'custom_views_post_title_shortcode_handler_on_single_page' );
}

function custom_views_post_title_shortcode_handler_on_single_page( $tag ) {

// create me a 2 digits increment
for ($x = 1; $x <= 100; $x++) {
$x = str_pad($x, 2, 0, STR_PAD_LEFT);
//echo "<option value = $x>$x</option>";

$title = "";
$output .= '<input type="text" value="'. $x .'" readonly>';
return $output;


}
}
add_action( 'wpcf7_init', 'custom_views_post_title_on_single_page' );


//$session->set_userdata( 'username', 'john' );
//$session->userdata( 'username' );
function custom_views_hidden() {
wpcf7_add_shortcode( 'custom_views_hidden', 'custom_views_post_title_shortcode_handler_on_single_page_hidden' );
}

function custom_views_post_title_shortcode_handler_on_single_page_hidden( $tag ) {
//$today = date("F j, Y");
//$dateExisted = $wpdb->get_results("SELECT * FROM dl_tps_forms_new WHERE date_created = $today");
date_default_timezone_set("Australia/Brisbane");
$today = date("F j, Y");
// $today = "December 19, 2017";
$date_created = date('dmY', strtotime($today));

// for ($x = 1; $x <= 1; $x++) {
$x = str_pad($x, 2, 0, STR_PAD_LEFT);
global $wpdb;
// $check_ifsaves =  $wpdb->get_results("SELECT * FROM dl_tps_forms_new WHERE data = 01 order by DESC limit 1 ");
// if($check_ifsaves){
$dateExisted = $wpdb->get_results("SELECT * FROM dl_tps_forms_new WHERE date_created = '".$today."' ");
if($wpdb->num_rows > 0){
$sum = 0;
$sum =  $wpdb->get_var("SELECT COUNT(data) as Total FROM dl_tps_forms_new WHERE date_created  = '".$today."'");
$x = $sum + 1;
$x = str_pad($x, 2, 0, STR_PAD_LEFT);

$random_number = intval( "0" .rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9)  . rand(0,9)  . rand(0,9)  . rand(0,9) );
$wp_session = WP_Session::get_instance();
$wp_session['user_name'] = $random_number;
$mahsession =  $wp_session['user_name'];

// $wp_session = WP_Session::get_instance();
// $mysession = $wp_session['user_name'];
// $mahsession = $mysession;

$output .= '<input type="hidden" id="session_no" name="session_no" value="'.$mahsession.'" readonly>';
$output .= '<input type="hidden" id="customerno" name="item_name" value="'.$x.'" readonly>';
$output .= '<input type="hidden" id="blank" name="trackernum" value="EA'.$date_created.$x.'" readonly>';


// }
//}
}else{
$x =  1;
$x = str_pad($x, 2, 0, STR_PAD_LEFT);
$random_number = intval( "0" .rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9)  . rand(0,9)  . rand(0,9)  . rand(0,9) );
$wp_session = WP_Session::get_instance();
$wp_session['user_name'] = $random_number;
$mahsession =  $wp_session['user_name'];

// $wp_session = WP_Session::get_instance();
// $mysession = $wp_session['user_name'];
// $mahsession = $mysession;

$output .= '<input type="hidden" id="session_no" name="session_no" value="'.$mahsession.'" readonly>';

$output .= '<input type="hidden" id="customerno" name="item_name" value="'.$x.'" readonly>';
$output .= '<input type="hidden" id="blank" name="trackernum" value="EA'.$date_created.$x.'" readonly>';
// }
}
$output .= '<input type="hidden" id="item_name" name="item_name" value="E" readonly>';
$output .= '<input type="hidden" id="air" name="item_name" value="A" readonly>';
$output .= '<input type="hidden" id="airtitle" name="item_name" value="Online Customs Air Freight" readonly>';
$output .= '<input type="hidden" id="item_amount" name="amount" value="150" readonly>';
// $output .= '<input type="hidden" name="cmd" value="_s-xclick" readonly>';
//  $output .= '<input type="hidden" name="hosted_button_id" value="Y7S77U5QVUGXA" readonly>';
$output .= '<input type="hidden"  id="formdate" name="item_name" value="'.$date_created.'" readonly>';
return $output;

}
add_action( 'wpcf7_init', 'custom_views_hidden' );
function custom_views_hidden_radio() {
wpcf7_add_shortcode( 'custom_views_hidden_radio', 'custom_views_post_title_shortcode_handler_on_single_page_hidden_radio' );
}

function custom_views_post_title_shortcode_handler_on_single_page_hidden_radio( $tag ) {
date_default_timezone_set("Australia/Brisbane");
$today = date("F j, Y, g:i a");
$date_created = date('dmY', strtotime($today));


//$output .= '<input type="hidden" id="business" name="business" value="'. $x .'" readonly>';
$output .= '<input type="radio" id="yes" name="charge" value="308" > Yes ';   //308
$output .= '<input type="radio" id="no" name="charge" value="198" checked> No';  // 198
$output .= '<span style="display:none;"><input type="radio"  name="charge1" value="0" checked></span>';
$output .= '<span style="display:none;"><input type="radio"  name="charge2" value="0" checked></span>';
//$output .= '<span style="display:none;"><input type="radio"  name="charge2" value="350" checked></span>';
//$output .= '<span style="display:none;"><input type="radio"  name="charge2" value="385"></span>';
// $output .= '<input type="hidden" name="cmd" value="_s-xclick" readonly>';
//  $output .= '<input type="hidden" name="hosted_button_id" value="Y7S77U5QVUGXA" readonly>';

return $output;

}


add_action( 'wpcf7_init', 'custom_views_hidden_250' );
function custom_views_hidden_250() {
wpcf7_add_shortcode( 'custom_views_hidden_250', 'custom_views_post_title_shortcode_handler_on_single_page_hidden_radio_250' );
}

function custom_views_post_title_shortcode_handler_on_single_page_hidden_radio_250( $tag ) {
date_default_timezone_set("Australia/Brisbane");
$today = date("F j, Y, g:i a");
$date_created = date('dmY', strtotime($today));


//$output .= '<input type="hidden" id="business" name="business" value="'. $x .'" readonly>';
$output .= '<input type="radio" id="yes" name="charge" value="275" > Yes '; //  275
$output .= '<input type="radio" id="no" name="charge" value="0" checked> No'; // 0
// $output .= '<input type="hidden" name="cmd" value="_s-xclick" readonly>';
//  $output .= '<input type="hidden" name="hosted_button_id" value="Y7S77U5QVUGXA" readonly>';

return $output;

}


add_action( 'wpcf7_init', 'custom_views_hidden_radio' );
// Online custom sea freight
function custom_views_hidden_sea_freight() {
wpcf7_add_shortcode( 'custom_views_hidden_sea_freight', 'custom_views_post_title_shortcode_handler_on_single_page_hidden_sea' );
}

function custom_views_post_title_shortcode_handler_on_single_page_hidden_sea( $tag ) {
date_default_timezone_set("Australia/Brisbane");
$today = date("F j, Y");
// $today = "December 19, 2017";
$date_created = date('dmY', strtotime($today));

// for ($x = 1; $x <= 1; $x++) {
$x = str_pad($x, 2, 0, STR_PAD_LEFT);
global $wpdb;

$dateExisted = $wpdb->get_results("SELECT * FROM dl_tps_forms_new WHERE date_created = '".$today."' ");
if($wpdb->num_rows > 0){
$sum = 0;
$sum =  $wpdb->get_var("SELECT COUNT(data) as Total FROM dl_tps_forms_new WHERE date_created  = '".$today."'");
$x = $sum + 1;
$x = str_pad($x, 2, 0, STR_PAD_LEFT);

$random_number = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9)  . rand(0,9)  . rand(0,9)  . rand(0,9) );
$wp_session = WP_Session::get_instance();
$wp_session['user_name'] = $random_number;
$mahsession =  $wp_session['user_name'];

// $wp_session = WP_Session::get_instance();
// $mysession = $wp_session['user_name'];
// $mahsession = $mysession;

$output .= '<input type="hidden" id="session_no" name="session_no" value="'.$mahsession.'" readonly>';
$output .= '<input type="hidden" id="customerno" name="item_name" value="'.$x.'" readonly>';
$output .= '<input type="hidden" id="blank" name="trackernum" value="ES'.$date_created.$x.'" readonly>';

}else{
$x =  1;
$x = str_pad($x, 2, 0, STR_PAD_LEFT);
$random_number = intval( "0" .rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9)  . rand(0,9)  . rand(0,9)  . rand(0,9) );
$wp_session = WP_Session::get_instance();
$wp_session['user_name'] = $random_number;
$mahsession =  $wp_session['user_name'];

// $wp_session = WP_Session::get_instance();
// $mysession = $wp_session['user_name'];
// $mahsession = $mysession;


$output .= '<input type="hidden" id="session_no" name="session_no" value="'.$mahsession.'" readonly>';
$output .= '<input type="hidden" id="customerno" name="item_name" value="'.$x.'" readonly>';
$output .= '<input type="hidden" id="blank" name="trackernum" value="ES'.$date_created.$x.'" readonly>';
// }
}
$output .= '<input type="hidden" id="item_name" name="item_name" value="E" readonly>';
$output .= '<input type="hidden" id="air" name="item_name" value="S" readonly>';
$output .= '<input type="hidden" id="airtitle" name="item_name" value="Online Customs Sea Freight" readonly>';
$output .= '<input type="hidden" id="item_amount" name="amount" value="150" readonly>';
// $output .= '<input type="hidden" name="cmd" value="_s-xclick" readonly>';
//  $output .= '<input type="hidden" name="hosted_button_id" value="Y7S77U5QVUGXA" readonly>';
$output .= '<input type="hidden"  id="formdate" name="item_name" value="'.$date_created.'" readonly>';
return $output;

}
add_action( 'wpcf7_init', 'custom_views_hidden_sea_freight' );
//hidden field
function custom_views_hidden_personal_effects() {
wpcf7_add_shortcode( 'custom_views_hidden_personal_effects', 'custom_views_post_title_shortcode_handler_on_single_page_hidden_personal_effects' );
}

function custom_views_post_title_shortcode_handler_on_single_page_hidden_personal_effects( $tag ) {
date_default_timezone_set("Australia/Brisbane");
$today = date("F j, Y");
//$exactTodate = date("F j, Y  h:i:s A");
$date_created = date('dmY', strtotime($today));

//   for ($x = 1; $x <= 1; $x++) {
//    $x = str_pad($x, 2, 0, STR_PAD_LEFT);
//$output .= '<input type="hidden" id="business" name="business" value="'. $x .'" readonly>';
//    $output .= '<input type="hidden" id="customerno" name="item_name" value="'. $x .'" readonly>';
// }
global $wpdb;

$dateExisted = $wpdb->get_results("SELECT * FROM dl_tps_forms_new WHERE date_created = '".$today."' ");
if($wpdb->num_rows > 0){
$sum = 0;
$sum =  $wpdb->get_var("SELECT COUNT(data) as Total FROM dl_tps_forms_new WHERE date_created  = '".$today."'");
$x = $sum + 1;
$x = str_pad($x, 2, 0, STR_PAD_LEFT);

$random_number = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9)  . rand(0,9)  . rand(0,9)  . rand(0,9) );
$wp_session = WP_Session::get_instance();
$wp_session['user_name'] = $random_number;
$mahsession =  $wp_session['user_name'];

// $wp_session = WP_Session::get_instance();
// $mysession = $wp_session['user_name'];
// $mahsession = $mysession;

$output .= '<input type="hidden" id="session_no" name="session_no" value="'.$mahsession.'" readonly>';
$output .= '<input type="hidden" id="customerno" name="item_name" value="'.$x.'" readonly>';
$output .= '<input type="hidden" id="blank" name="trackernum" value="PE'.$date_created.$x.'" readonly>';

}else{
$x =  1;
$x = str_pad($x, 2, 0, STR_PAD_LEFT);
$random_number = intval( "0" .rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9)  . rand(0,9)  . rand(0,9)  . rand(0,9) );
$wp_session = WP_Session::get_instance();
$wp_session['user_name'] = $random_number;
$mahsession =  $wp_session['user_name'];
// $wp_session = WP_Session::get_instance();
// $mysession = $wp_session['user_name'];
// $mahsession = $mysession;


$output .= '<input type="hidden" id="session_no" name="session_no" value="'.$mahsession.'" readonly>';
$output .= '<input type="hidden" id="customerno" name="item_name" value="'.$x.'" readonly>';
$output .= '<input type="hidden" id="blank" name="trackernum" value="PE'.$date_created.$x.'" readonly>';
// }
}
$output .= '<input type="hidden" id="item_name" name="item_name" value="E" readonly>';
$output .= '<input type="hidden" id="air" name="item_name" value="PE" readonly>';
$output .= '<input type="hidden" id="airtitle" name="item_name" value="Online Customs Personal Effects" readonly>';
$output .= '<input type="hidden" id="item_amount" name="amount" value="150" readonly>';
// $output .= '<input type="hidden" name="cmd" value="_s-xclick" readonly>';
//  $output .= '<input type="hidden" name="hosted_button_id" value="Y7S77U5QVUGXA" readonly>';
$output .= '<input type="hidden"  id="formdate" name="item_name" value="'. $date_created .'" readonly>';
return $output;

}
add_action( 'wpcf7_init', 'custom_views_hidden_personal_effects' );
function custom_views_hidden_radio_personal_effects() {
wpcf7_add_shortcode( 'custom_views_hidden_radio_personal_effects', 'custom_views_post_title_shortcode_handler_on_single_page_hidden_radio_effects' );
}

function custom_views_post_title_shortcode_handler_on_single_page_hidden_radio_effects( $tag ) {
date_default_timezone_set("Australia/Brisbane");
$today = date("F j, Y, g:i a");
$date_created = date('dmY', strtotime($today));


//$output .= '<input type="hidden" id="business" name="business" value="'. $x .'" readonly>';
$output .= '<input type="radio" id="yessir" name="charge1" value="605" > Yes ';  // $605
$output .= '<input type="radio" id="nosir" name="charge1" value="0.00" checked> No';  // $0.00
$output .= '<span style="display:none;"><input type="radio"  name="charge2" value="385" checked></span>';  //385
// $output .= '<input type="hidden" name="cmd" value="_s-xclick" readonly>';
//  $output .= '<input type="hidden" name="hosted_button_id" value="Y7S77U5QVUGXA" readonly>';
$output .= '<input type="hidden" name="amount" id="Bform"  value="350" >'; // 350
$output .= '<input type="hidden" id="formalities"  value="250" >';    //250
//  $output .= '<input type="radio" name="declare" value="550" >';

return $output;

}
add_action( 'wpcf7_init', 'custom_views_hidden_radio_personal_effects' );



//Export Declaration
function custom_views_hidden_export_dec() {
wpcf7_add_shortcode( 'custom_views_hidden_export_dec', 'custom_views_post_title_shortcode_handler_on_single_page_hidden_export_dec' );
}

function custom_views_post_title_shortcode_handler_on_single_page_hidden_export_dec( $tag ) {

date_default_timezone_set("Australia/Brisbane");
$today = date("F j, Y");
// $today = "December 19, 2017";
$date_created = date('dmY', strtotime($today));

// for ($x = 1; $x <= 1; $x++) {
$x = str_pad($x, 2, 0, STR_PAD_LEFT);
global $wpdb;

$dateExisted = $wpdb->get_results("SELECT * FROM dl_tps_forms_new WHERE date_created = '".$today."' ");
if($wpdb->num_rows > 0){
$sum = 0;
$sum =  $wpdb->get_var("SELECT COUNT(data) as Total FROM dl_tps_forms_new WHERE date_created  = '".$today."'");
$x = $sum + 1;
$x = str_pad($x, 2, 0, STR_PAD_LEFT);

$random_number = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9)  . rand(0,9)  . rand(0,9)  . rand(0,9) );
$wp_session = WP_Session::get_instance();
$wp_session['user_name'] = $random_number;
$mahsession =  $wp_session['user_name'];

// $wp_session = WP_Session::get_instance();
// $mysession = $wp_session['user_name'];
// $mahsession = $mysession;

//$output .= '<input type="hidden" id="business" name="business" value="'. $x .'" readonly>';
$output .= '<input type="hidden" id="session_no" name="session_no" value="'.$mahsession.'" readonly>';
$output .= '<input type="hidden" id="customerno" name="item_name" value="'.$x.'" readonly>';
$output .= '<input type="hidden" id="blank" name="trackernum" value="EDN'.$date_created.$x.'" readonly>';
}else{
  $x =  1;
$x = str_pad($x, 2, 0, STR_PAD_LEFT);
$random_number = intval( "0" .rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9)  . rand(0,9)  . rand(0,9)  . rand(0,9) );
$wp_session = WP_Session::get_instance();
$wp_session['user_name'] = $random_number;
$mahsession =  $wp_session['user_name'];

// $wp_session = WP_Session::get_instance();
// $mysession = $wp_session['user_name'];
// $mahsession = $mysession;

$output .= '<input type="hidden" id="session_no" name="session_no" value="'.$mahsession .'" readonly>';
$output .= '<input type="hidden" id="customerno" name="item_name" value="'.$x.'" readonly>';
$output .= '<input type="hidden" id="blank" name="trackernum" value="EDN'.$date_created.$x.'" readonly>';
}
$output .= '<input type="hidden" id="session_no" name="session_no" value="'.$mahsession.'" readonly>';
$output .= '<input type="hidden" id="item_name" name="item_name" value="E" readonly>';
$output .= '<input type="hidden" id="air" name="item_name" value="EDN" readonly>';
$output .= '<input type="hidden" id="airtitle" name="item_name" value="Online Export Declaration" readonly>';

//$output .= '<input type="hidden" id="item_amount" name="amount" value="150" readonly>';
// $output .= '<input type="hidden" name="cmd" value="_s-xclick" readonly>';
//  $output .= '<input type="hidden" name="hosted_button_id" value="Y7S77U5QVUGXA" readonly>';
$output .= '<input type="hidden"  id="formdate" name="item_name" value="'.$date_created.'" readonly>';
return $output;

}
add_action( 'wpcf7_init', 'custom_views_hidden_export_dec' );
function custom_views_hidden_radio_ed() {
wpcf7_add_shortcode( 'custom_views_hidden_radio_ed', 'custom_views_post_title_shortcode_handler_on_single_page_hidden_radio_ed' );
}

function custom_views_post_title_shortcode_handler_on_single_page_hidden_radio_ed( $tag ) {
date_default_timezone_set("Australia/Brisbane");
$today = date("F j, Y, g:i a");
$date_created = date('dmY', strtotime($today));


//$output .= '<input type="hidden" id="business" name="business" value="'. $x .'" readonly>';
$output .= '<input type="radio" id="yes" name="charge" value="71.50" checked style="display:none;">  ';  //71.50
$output .= '<span style="display:none;"><input type="radio"  name="charge1" value="0" checked></span>';
$output .= '<span style="display:none;"><input type="radio"  name="charge2" value="0" checked></span>';
//$output .= '<input type="radio" id="no" name="charge" value="180" > No';
// $output .= '<input type="hidden" name="cmd" value="_s-xclick" readonly>';
//  $output .= '<input type="hidden" name="hosted_button_id" value="Y7S77U5QVUGXA" readonly>';
//value="71.50
return $output;

}
add_action( 'wpcf7_init', 'custom_views_hidden_radio_ed' );
/* Australian Post */
function sssssstarttt_tt() {
wpcf7_add_shortcode( 'sssssstarttt_tt', 'xsjsjsjsww' );
}

function xsjsjsjsww( $tag ) {
date_default_timezone_set("Australia/Brisbane");
$today = date("F j, Y");
$date_created = date('dmY', strtotime($today));

//for ($x = 1; $x <= 1; $x++) {
$x = str_pad($x, 2, 0, STR_PAD_LEFT);
global $wpdb;

$dateExisted = $wpdb->get_results("SELECT * FROM dl_tps_forms_new WHERE date_created = '".$today."' ");
if($wpdb->num_rows > 0){
$sum = 0;
$sum =  $wpdb->get_var("SELECT COUNT(data) as Total FROM dl_tps_forms_new WHERE date_created  = '".$today."'");
$x = $sum + 1;
$x = str_pad($x, 2, 0, STR_PAD_LEFT);

$random_number = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9)  . rand(0,9)  . rand(0,9)  . rand(0,9) );
$wp_session = WP_Session::get_instance();
$wp_session['user_name'] = $random_number;
$mahsession =  $wp_session['user_name'];

// $wp_session = WP_Session::get_instance();
// $mysession = $wp_session['user_name'];
// $mahsession = $mysession;

$output .= '<input type="hidden" id="session_no" name="session_no" value="'.$mahsession.'" readonly>';
$output .= '<input type="hidden" id="customerno" name="item_name" value="'.$x.'" readonly>';
$output .= '<input type="hidden" id="blank" name="trackernum" value="EP'.$date_created.$x.'" readonly>';

}else{
$x =  1;
$x = str_pad($x, 2, 0, STR_PAD_LEFT);
$random_number = intval( "0" .rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9)  . rand(0,9)  . rand(0,9)  . rand(0,9) );
$wp_session = WP_Session::get_instance();
$wp_session['user_name'] = $random_number;
$mahsession =  $wp_session['user_name'];

// $wp_session = WP_Session::get_instance();
// $mysession = $wp_session['user_name'];
// $mahsession = $mysession;

$output .= '<input type="hidden" id="session_no" name="session_no" value="'.$mahsession.'" readonly>';
$output .= '<input type="hidden" id="customerno" name="item_name" value="'.$x.'" readonly>';
$output .= '<input type="hidden" id="blank" name="trackernum" value="EP'.$date_created.$x.'" readonly>';
// }
}
//$output .= '<input type="hidden" id="business" name="business" value="'. $x .'" readonly>';
// $output .= '<input type="hidden" id="customerno" name="item_name" value="'. $x .'" readonly>';
//}
$output .= '<input type="hidden" id="item_name" name="item_name" value="E" readonly>';
$output .= '<input type="hidden" id="air" name="item_name" value="P" readonly>';
$output .= '<input type="hidden" id="airtitle" name="item_name" value="Online Customs Clearance Australian Post" readonly>';
//$output .= '<input type="hidden" id="item_amount" name="amount" value="150" readonly>';
// $output .= '<input type="hidden" name="cmd" value="_s-xclick" readonly>';
//  $output .= '<input type="hidden" name="hosted_button_id" value="Y7S77U5QVUGXA" readonly>';
$output .= '<input type="hidden"  id="formdate" name="item_name" value="'.$date_created.'" readonly>';
return $output;

}
add_action( 'wpcf7_init', 'sssssstarttt_tt' );


function custom_views_hidden_radio_australian_post() {
wpcf7_add_shortcode( 'custom_views_hidden_radio_australian_post', 'custom_views_post_title_shortcode_handler_on_single_page_hidden_radio_australian_post' );
}

function custom_views_post_title_shortcode_handler_on_single_page_hidden_radio_australian_post( $tag ) {
date_default_timezone_set("Australia/Brisbane");
$today = date("F j, Y");
$date_created = date('dmY', strtotime($today));


//$output .= '<input type="hidden" id="business" name="business" value="'. $x .'" readonly>';
$output .= '<input type="radio" id="yes" name="charge" value="225.50" > Yes ';   //225.50
$output .= '<input type="radio" id="no" name="charge" value="143" checked> No';  //143
$output .= '<span style="display:none;"><input type="radio"  name="charge1" value="0" checked></span>';
$output .= '<span style="display:none;"><input type="radio"  name="charge2" value="0" checked></span>';
// $output .= '<input type="hidden" name="cmd" value="_s-xclick" readonly>';
//  $output .= '<input type="hidden" name="hosted_button_id" value="Y7S77U5QVUGXA" readonly>';

return $output;

}
add_action( 'wpcf7_init', 'custom_views_hidden_radio_australian_post' );




add_action('wpcf7_before_send_mail', 'save_form123' );
function save_form123( $wpcf7 ) {
global $wpdb;

/*
Note: since version 3.9 Contact Form 7 has removed $wpcf7->posted_data
and now we use an API to get the posted data.
*/
$submission = WPCF7_Submission::get_instance();
if ( $submission ) {

$submited = array();
$submited['title'] = $wpcf7->title();
$submited['posted_data'] = $submission->get_posted_data();

}
$data = array(
'name'  => $submited['posted_data']['text-350'],
'info' => $submited['posted_data']['item_name'],
'charge' => $submited['posted_data']['charge'],
'given_name' => $submited['posted_data']['given_name'],
'f_name' => $submited['posted_data']['f_name'],
'address_phone' => $submited['posted_data']['address_phone'],
'dob' => $submited['posted_data']['dob'],

);
for ($x = 1; $x <= 1; $x++) {
$x = str_pad($x, 2, 0, STR_PAD_LEFT);
date_default_timezone_set("Australia/Brisbane");
$today = date("F j, Y");
$date_created = date('dmY', strtotime($today));
//$exactTodate = date("F j, Y  h:i:s A");
//$today = "December 19, 2017";
// $today = "December 20, 2017";

$dateExisted = $wpdb->get_results("SELECT * FROM dl_tps_forms_new WHERE date_created = '".$today."' ");
if($wpdb->num_rows > 0){
$check_ifsaves =  $wpdb->get_results("SELECT * FROM dl_tps_forms_new WHERE data = 01 limit 1 ");
if($check_ifsaves){
$sum = 0;
$sum =  $wpdb->get_var("SELECT COUNT(data) as Total FROM dl_tps_forms_new WHERE date_created  = '".$today."' ");
$x = $sum + 1;
$x = str_pad($x, 2, 0, STR_PAD_LEFT);

//Export Dex
 if(isset($_POST['email-540'])){
    $wpdb->insert( $wpdb->prefix . 'tps_forms_new',
array(
'form'  => $submited['title'],
'data' => $x,
'date_created' => $today,
'amount' => serialize($data),
'status' => 'pending',
'tracknumber' => 'EDN'.$date_created.$x,
'session_data' => $_POST['session_no'],
'email' => $_POST['email-540'],
'vessel_number' =>$_POST['text-418'],
'date_depart'=> $_POST['date-309']
)
);
  }

  // Personal effects
    if(isset($_POST['email-442'])){
                      $wpdb->insert( $wpdb->prefix . 'tps_forms_new',
                  array(
                  'form'  => $submited['title'],
                  'data' => $x,
                  'date_created' => $today,
                  'amount' => serialize($data),
                  'status' => 'pending',
                  'tracknumber' => 'PE'.$date_created.$x,
                  'session_data' => $_POST['session_no'],
                  'name' => $_POST['text-350'],
                  'email' => $_POST['email-442'],
                  'street_add' => $_POST['text-201'],
                  'family_name' =>$_POST['text-476'],
                  'phone'=> $_POST['number-886']
                  )
                  );
                    }



  			$EA = 'EA';
            $ES = 'ES';
            $EP = 'EP';
           $formID = $_POST['trackernum'];
          $subject1 = strpos($formID, $EA);
          $subject2 = strpos($formID, $ES);
          $subject3 = strpos($formID, $EP);
            if( $subject1 !== false ):
          $subject = "EA";
          endif;
          if( $subject2 !== false ):
          $subject = "ES";
          endif;
          if( $subject3 !== false ):
          $subject = "EP";
          endif;
       

$wpdb->insert( $wpdb->prefix . 'tps_forms_new',
array(
'form'  => $submited['title'],
'data' => $x,
'date_created' => $today,
'amount' => serialize($data),
'abn' => $_POST['tel-942'],
'tracknumber' => $subject.$date_created.$x,
'status' => 'pending',
'session_data' => $_POST['session_no'],
'email' => $_POST['email-925'],
'name' => $_POST['text-350'],
'family_name' => $_POST['text-476'],
'company_name' => $_POST['text-201'],
'street_add' => $_POST['text-243'],
'phone' => $_POST['tel-864'],
'notes' => $_POST['textarea-860']
// 'exact_todate' => $exactTodate
)
);

// 
$wpdb->insert( $wpdb->prefix . 'dragndrop_f',
array(
'form'  => $submited['title'],
'tracknumber' => $subject.$date_created.$x,
'session_data' => $_POST['session_no'],
'date_created' =>  $today
)
);
}else{

		    
//Export Dex
   if(isset($_POST['email-540'])){
    $wpdb->insert( $wpdb->prefix . 'tps_forms_new',
array(
'form'  => $submited['title'],
'data' => $x,
'date_created' => $today,
'amount' => serialize($data),
'status' => 'pending',
'tracknumber' =>'EDN'.$date_created.$x,
'session_data' => $_POST['session_no'],
'email' => $_POST['email-540'],
'vessel_number' =>$_POST['text-418'],
'date_depart'=> $_POST['date-309']
)
);
  }

    if(isset($_POST['email-442'])){
                      $wpdb->insert( $wpdb->prefix . 'tps_forms_new',
                  array(
                  'form'  => $submited['title'],
                  'data' => $x,
                  'date_created' => $today,
                  'amount' => serialize($data),
                  'status' => 'pending',
                  'tracknumber' => 'PE'.$date_created.$x,
                  'session_data' => $_POST['session_no'],
                  'name' => $_POST['text-350'],
                  'email' => $_POST['email-442'],
                  'street_add' => $_POST['text-201'],
                  'family_name' =>$_POST['text-476'],
                  'phone'=> $_POST['number-886']
                  )
                  );
                    }

            $EA = 'EA';
            $ES = 'ES';
            $EP = 'EP';
           $formID = $_POST['trackernum'];
          $subject1 = strpos($formID, $EA);
          $subject2 = strpos($formID, $ES);
          $subject3 = strpos($formID, $EP);
            if( $subject1 !== false ):
          $subject = "EA";
          endif;
          if( $subject2 !== false ):
          $subject = "ES";
          endif;
          if( $subject3 !== false ):
          $subject = "EP";
          endif;


$wpdb->insert( $wpdb->prefix . 'tps_forms_new',
array(
'form'  => $submited['title'],
'data' => $x,
'date_created' => $today,
'amount' => serialize($data),
'abn' => $_POST['tel-942'],
'tracknumber' => $subject.$date_created.$x,
'status' => 'pending',
'session_data' => $_POST['session_no'],
'email' => $_POST['email-925'],
'name' => $_POST['text-350'],
'family_name' => $_POST['text-476'],
'company_name' => $_POST['text-201'],
'street_add' => $_POST['text-243'],
'phone' => $_POST['tel-864'],
'notes' => $_POST['textarea-860']
// 'attachment' => $_POST['checkbox-330']
)
);

$wpdb->insert( $wpdb->prefix . 'dragndrop_f',
array(
'form'  => $submited['title'],
'tracknumber' => $subject.$date_created.$x,
'session_data' => $_POST['session_no'],
'date_created' =>  $today
)
);
}

}  else{

  //Export Dex
  if(isset($_POST['email-540'])){
    $wpdb->insert( $wpdb->prefix . 'tps_forms_new',
array(
'form'  => $submited['title'],
'data' => $x,
'date_created' => $today,
'amount' => serialize($data),
'status' => 'pending',
'tracknumber' => 'EDN'.$date_created.$x,
'session_data' => $_POST['session_no'],
'email' => $_POST['email-540'],
'vessel_number' =>$_POST['text-418'],
'date_depart'=> $_POST['date-309']
)
);

  }

    if(isset($_POST['email-442'])){
                      $wpdb->insert( $wpdb->prefix . 'tps_forms_new',
                  array(
                  'form'  => $submited['title'],
                  'data' => $x,
                  'date_created' => $today,
                  'amount' => serialize($data),
                  'status' => 'pending',
                  'tracknumber' => 'PE'.$date_created.$x,
                  'session_data' => $_POST['session_no'],
                  'name' => $_POST['text-350'],
                  'email' => $_POST['email-442'],
                  'street_add' => $_POST['text-201'],
                  'family_name' =>$_POST['text-476'],
                  'phone'=> $_POST['number-886']
                  )
                  );
                    }

            $EA = 'EA';
            $ES = 'ES';
            $EP = 'EP';
           $formID = $_POST['trackernum'];
          $subject1 = strpos($formID, $EA);
          $subject2 = strpos($formID, $ES);
          $subject3 = strpos($formID, $EP);
            if( $subject1 !== false ):
          $subject = "EA";
          endif;
          if( $subject2 !== false ):
          $subject = "ES";
          endif;
          if( $subject3 !== false ):
          $subject = "EP";
          endif;


$wpdb->insert( $wpdb->prefix . 'tps_forms_new',
array(
'form'  => $submited['title'],
'data' => $x,
'date_created' => $today,
'amount' => serialize($data),
'abn' => $_POST['tel-942'],
'status' => 'pending',
'tracknumber' => $subject.$date_created.$x,
'session_data' => $_POST['session_no'],
'email' => $_POST['email-925'],
'name' => $_POST['text-350'],
'family_name' => $_POST['text-476'],
'company_name' => $_POST['text-201'],
'street_add' => $_POST['text-243'],
'phone' => $_POST['tel-864'],
'notes' => $_POST['textarea-860']

//'attachment' => $_POST['checkbox-330']
)
);

$wpdb->insert( $wpdb->prefix . 'dragndrop_f',
array(
'form'  => $submited['title'],
'tracknumber' => $subject.$date_created.$x,
'session_data' => $_POST['session_no'],
'date_created' =>  $today
)
);
}
// date checker
}
}

/* Sending to client email */
function foobar_func( $atts ){


  global $wpdb;
  $wp_session = WP_Session::get_instance();
  $mysession = $wp_session['user_name'];    







  $dontRepeat = $wpdb->get_results("SELECT * FROM tezt WHERE session_data = $mysession order by session_data DESC ");
  foreach($dontRepeat as $filter){
    $isSaved = $filter->transaction_id;
      if($isSaved){
      echo "<script>window.location='https://depthlogistics.com/capabilities/customs-clearance/'</script>";
      die();
    }
  }



$wp_session = WP_Session::get_instance();
//$wp_session['user_name'] = $random_number;
$mysession =  $wp_session['user_name'];
$getValue = $wpdb->get_results("SELECT * FROM dl_tps_forms_new WHERE session_data = $mysession order by tracknumber,name DESC");



foreach($getValue as $getz){
  $tracknumber = $getz->tracknumber;
  $name = $getz->name;
  $thismysession = $getz->session_data;

}
  date_default_timezone_set("Australia/Brisbane");
  $today = date("F j, Y, g:i a");


   $statusNow = $_GET['st'];
//   $statusNow = (string)$statusNow;
   $tx = $_GET['tx'];
   $amount = $_GET['amt'];
   $cc = $_GET['cc'];
   
   $ss  = $wpdb->insert( 'tezt', 
   array( 
                           'status'  => $statusNow, 
                           'transaction_id' => $tx,
                           'amount' => $amount,
                           'currency' => $cc,
                           'session_data' => $thismysession,
                           'tracknumber' => $tracknumber,
                           'name' => $name,
                           'date_created' => $today
                         
                        )
                    );



 //$wpdb->query($wpdb->prepare("UPDATE dl_db7_forms SET session_data='$mysession' WHERE form_post_id = 76197 "));
// Send email for pending payments / failed to pay
$resultz = $wpdb->get_results("SELECT * FROM tezt WHERE session_data = $mysession order by status,transaction_id DESC ");
if($wpdb->num_rows > 0){        
         

 $checkerrz = $wpdb->get_results("SELECT * FROM dl_tps_forms_new WHERE session_data=$mysession");
          foreach( $checkerrz as $check ){
          	$mysess = $check->session_data;
            $formID = $check->tracknumber;
            $EA = 'EA';
            $ES = 'ES';
            $EP = 'EP';
            $EDN = 'EDN';
            $PE = 'PE';
          }

          $subject1 = strpos($formID, $EA);
          $subject2 = strpos($formID, $ES);
          $subject3 = strpos($formID, $EP);
          $subject4 = strpos($formID, $EDN);
          $subject5 = strpos($formID, $PE);

if( $subject1 !== false OR $subject2 !== false OR $subject3 !== false  ){

	 $result = $wpdb->get_results("SELECT * FROM dl_dragndrop_f WHERE session_data = $mysession order by date_created,form,tracknumber");
}

if( $subject4 !== false OR $subject5 !== false ){
 
  $result = $wpdb->get_results("SELECT * FROM dl_db7_forms WHERE session_data = $mysession order by form_value DESC ");
 }

 if($wpdb->num_rows > 0){
 
     foreach( $result as $res ){

       $wp_session = WP_Session::get_instance();
       $mysession = $wp_session['user_name'];

       // $to = $res->email;
       // $name =$res->name;
        $tracknumber = $res->tracknumber;
   

                          $wpdb->update( 
                'dl_tps_forms_new', 
               array(
                'status' => $statusNow,
                'transaction_id' => $tx), //data
                array('session_data' => $mysession), //where
                array('%s'), //data format
                array('%s') //where format
              );

            $queriestList = $wpdb->get_results("SELECT * FROM dl_tps_forms_new WHERE session_data=$mysession");

            foreach( $queriestList as $value ){

            $tracknumber = $value->tracknumber;
            $name = $value->name;
            $family_name = $value->family_name;
            $company_name = $value->company_name;
            $ABN = $value->abn;
            $street_add = $value->street_add;
            $phone = $value->phone;
            $status = $value->status;
            $session_no = $value->session_data;
            $email = $value->email;
            $notes = $value->notes;
            $vessel_no = $value->vessel_number;
            $depart_date = $value->date_depart;
            $airway_bill = $value->tracknumber;
            $marine_insurance = $value->tracknumber;
            $packing_list = $value->tracknumber;
            $certificate_of_origin = $value->tracknumber;
            $supplier_comm_invoice = $value->tracknumber;
            $photo_identification = $value->tracknumber;
            $quarantine_treatment = $value->tracknumber;
            $other_document = $value->tracknumber;
            }



           $files = array($var,$var1,$var2,$var3,$var4,$var5,$var6,$var7);
          // ob_start(); 
          //$content = get_template_part('forms/air-freight.php');
          $to = "brad@depthindustries.com,julie.magnone@gmail.com,mikky@depthindustries.com,$email"; //brad@depthindustries.com,julie.magnone@gmail.com,$email 
          $checker = $wpdb->get_results("SELECT * FROM dl_tps_forms_new WHERE session_data=$mysession");
          foreach( $checker as $check ){
          	$mysess = $check->session_data;
            $formID = $check->tracknumber;
            $EA = 'EA';
            $ES = 'ES';
            $EP = 'EP';
            $EDN = 'EDN';
            $PE = 'PE';
          }
          $subject1 = strpos($formID, $EA);
          $subject2 = strpos($formID, $ES);
          $subject3 = strpos($formID, $EP);
          $subject4 = strpos($formID, $EDN);
          $subject5 = strpos($formID, $PE);
          if( $subject1 !== false ):
          $subject = "Online Air Freight Customs Clearance";
          endif;
          if( $subject2 !== false ):
          $subject = "Online Sea Freight Customs Clearance";
          endif;
          if( $subject3 !== false ):
          $subject = "Online Customs Clearance Australia Post";
          endif;
          if( $subject4 !== false ):
          $subject = "Online Export Declaration";
          endif;
          if( $subject5 !== false ):
          $subject = "Online Personal Effects Customs Clearance";
          endif;

//$formValue = array(1=>'EA',2=>'ES',3=>'EP',4=>'EDN',5=>'PE');
if( $subject1 !== false OR $subject2 !== false OR $subject3 !== false  ){
     endif;

 //  $nowz = $wpdb->get_results("SELECT * FROM dl_db7_forms WHERE session_data=$mysession");
   $recentt = $wpdb->get_results("SELECT * FROM dl_dragndrop_f WHERE session_data=$mysession order by data,attach1,attach2,attach3,attach4,attach5,attach6,attach7,attach8,attach9 ");


   			$muchvar = array();
           foreach( $recentt as $rec ){

           	$nfile  = $rec->data;
           	$nfile1 = $rec->attach1;
           	$nfile2 = $rec->attach2;
           	$nfile3 = $rec->attach3;
           	$nfile4 = $rec->attach4;
           	$nfile5 = $rec->attach5;
           	$nfile6 = $rec->attach6;
           	$nfile7 = $rec->attach7;
           	$nfile8 = $rec->attach8;
           	$nfile9 = $rec->attach9;


           	$at_name = $rec->attach_name;
           	$at_name1 = $rec->attach_name1;
           	$at_name2 = $rec->attach_name2;
           	$at_name3 = $rec->attach_name3;
           	$at_name4 = $rec->attach_name4;
           	$at_name5 = $rec->attach_name5;
           	$at_name6 = $rec->attach_name6;
           	$at_name7 = $rec->attach_name7;
           	$at_name8 = $rec->attach_name8;
           	$at_name9 = $rec->attach_name9;

            }


  $muchvar  = array($nfile,$nfile1,$nfile2,$nfile3,$nfile4,$nfile5,$nfile6,$nfile7,$nfile8,$nfile9);   



$at_name = $at_name.' | ';
$at_name1 = $at_name1.' | ';
$at_name2 = $at_name2.' | ';
$at_name3 = $at_name3.' | ';
$at_name4 = $at_name4.' | ';
$at_name5 = $at_name5.' | ';
$at_name6 = $at_name6.' | ';
$at_name7 = $at_name7.' | ';
$at_name8 = $at_name8.' | ';
$at_name9 = $at_name9.' | ';






  //$muchvar = array($file1,$file2,$file3,$file4,$file5,$file6,$file7,$file10);          
//$filename = "http://custom-clearance.depthstaging.com/wp-content/uploads/cfdb7_uploads/";
$from = "Depth Logistics - Phone 1DEPTH(13 37 84)<enquieries@depthlogistics.com>";
//$subject = "Test Attachment Email";

$separator = md5(time());

// carriage return type (we use a PHP end of line constant)
$eol = PHP_EOL;


$yearNow = date('Y');
$monNow = date('m');
$lastF = $yearNow.'/'.$monNow;

$path = 'https://depthlogistics.com/wp-content/uploads/'.$lastF;
//$path = 'https://depthlogistics.com/wp-content/uploads/cfdb7_uploads/';



$fillMe = array();

$multiAT = array($at_name,$at_name1,$at_name2,$at_name3,$at_name4,$at_name5,$at_name6,$at_name7,$at_name8,$at_name9);

foreach( $multiAT as $singleAT ){
	
	$fillMe[] = $singleAT;
}

// message
$body .= "--".$separator.$eol;
$body .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
$body .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
$body .= $message.$eol;

          $body .=  "<style type='text/css'>
 @font-face {font-family: 'Optima Regular';
  src: url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.eot'); /* IE9*/
  src: url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
  url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.woff2') format('woff2'), /* chrome?firefox */
  url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.woff') format('woff'), /* chrome?firefox */
  url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.ttf') format('truetype'), /* chrome?firefox?opera?Safari, Android, iOS 4.2+*/
  url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.svg#Optima Regular') format('svg'); /* iOS 4.1- */
}
  body{
      font-family: 'Optima Regular';
      /*font-size:medium;*/
  }
  p{
      font-family: 'Optima Regular'
    /*font-size:medium;   */
  }

       span.wpcf7-list-item-label{ font-size: inherit !important; }

    /* Form Number */
   .form-number{background-color:black;color:white;border-radius: 50%; padding-left:7px; padding-right:5px;/*font-size:medium;*/ font-family: 'Optima Regular'; }
   
    /* General */
  input[type='radio'] { display: inline; float: left; width: 18px;font-size:medium; }   
    input[type='text'] { width:100%; }  
  input[type='submit']{ /*background-color:rgb(51,122,183);*/color:white;border-color:1px solid rgb(46, 109, 164);font-size:medium; font-family: 'Optima Regular';}       
    .wpcf7-textarea { width:50% !important; height:100px;font-family: 'Optima Regular';}    
    .wpcf7-text { width:100% !important;  font-family: 'Optima Regular'; }
  /* Submit button */
  #submit-btn{
    /*background-color:rgb(51,122,183);*/
                background-color: #333;
    color:white;
    border-color:1px solid rgb(46, 109, 164);
    font-size:medium; font-family: 'Optima Regular';
  }
  .submit-btn:hover{
    background-color:rgb(40,96,144);            
    color:white;
    border-color:1px solid rgb(32, 77, 116);    
    font-size:medium; font-family: 'Optima Regular';    
  }       
  #chkAgreement{display:block; -webkit-appearance: checkbox; font-size:medium; font-family: 'Optima Regular';}
    /* Proposer */
  #f1-txtName { width:100%; /*font-size:medium;*/ font-family: 'Optima Regular';}
  #f1-txtAddress{ width:70%; /*font-size:medium;*/font-family: 'Optima Regular';}
  #f1-txtPostcode{ width:30%; /*font-size:medium;*/ font-family: 'Optima Regular';}
</style>
<span style='font-size: 16px;font-weight: 700; font-family: Optima;'>Dear $name,</span><br> <br> 

<span style='font-size: 16px; font-family: Optima; font-size: 15px;'>Thank you for choosing Depth Logistics. </span>

<p style='padding: 0px 0px;line-height: 22px;width: 750px; font-family: Optima; font-size: 15px;'>We have begun processing the customs clearance of your shipment and we will be back in touch very soon.</p>

<p style='padding: 0px 0px;line-height: 22px;width: 750px; font-family: Optima; font-size: 15px;'>Should you need to contact us we are available 24/7 by <a href='mailto:enquiries@depthlogistics.com'>email</a> and by toll free phone on 1DEPTH (133784). Please quote the tracking number contained in this message to make serving your request more efficient.</p>

<p style='padding: 0px 0px;line-height: 22px;width: 750px; font-family: Optima; font-size: 15px;'>Learn more about how long processing takes and duties and taxes are calculated <a href='https://depthlogistics.com/capabilities/customs-clearance/'>here</a>.</p>

<p style='line-height: 22px;width: 752px; font-family: Optima; font-size: 15px;'>Depth Logistics offers a broad range of other capabilities beyond customs clearance. If you require support with freight forwarding, quarantine and biosecurity, or have any other  logistics enquiry, please click <A href='https://depthlogistics.com/capabilities/'>here</a> to learn more.</p>  

<p style='line-height: 22px;width: 752px; font-family: Optima; font-size: 15px;'>Please find below a summary of the documents and instructions you have provided us. Should we or Customs and Quarantine have any queries we will contact you immediately.</p>

<p style='line-height: 22px;width: 752px; font-family: Optima; font-size: 15px;'>Should we or Customs and Quarantine have any queries we will contact you immediately.</p>
<br>
<table width='750' height='250' border='0' style='background-color:#dbe0e6;' >
<tr><td colspan='2' style='background: #1B273D; font-family: Optima;color: #fff;font-size: 25px;padding: 5px 5px;'>Details: <span style='padding-left: 70px; font-family: Optima;'> $subject </span></td></tr>
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>Tracking Number:</th></td> <td style='padding: 8px 7px;font-family: Optima;font-size: 18px; font-weight:bold;'> $tracknumber</td></tr> 

<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>Name:</td> <td style='background: #fff; padding: 8px 7px;font-family: Optima;font-size: 15px;'> $name </td></tr> 
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>Family Name:</th></td> <td style='padding: 8px 7px;font-family: Optima;font-size: 15px;'> $family_name </td></tr> 
<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>Company Name:</td> <td style='background: #fff; padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> 
$company_name </td></tr> 
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>ABN:</td> <td style=' padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> $ABN </td></tr> 
<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>Street Address:</td> <td style='background: #fff; padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> $street_add </td></tr> 
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>Phone:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> $phone</td></tr> 
<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>Email:</td> <td style='background: #fff; padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> $email
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>Other notes:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'>  </td></tr> 

<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>Files uploaded:</td> <td style='background: #fff; padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> 
$fillMe[0] $fillMe[1] $fillMe[2] $fillMe[3] $fillMe[4] $fillMe[5] $fillMe[6] $fillMe[7] $fillMe[8] $fillMe[9]
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>Other notes:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> $notes  </td></tr> 




</table><br><br>
<br>
Kind Regards, <br>
<br>
The Team at Depth Logistics<br>
<br>
<img src='https://depthlogistics.com/wp-content/uploads/2018/01/depth-logistics.png' style='width: 13%;'><br>
Toll Free: 1DEPTH (13 37 84)<br>
Email: <a href='mailto:enquiries@depthlogistics.com'>enquiries@depthlogistics.com</a><br>
<a href='https://depthlogistics.com/about-us/trading-terms/'>Terms and Conditions</a>
<a href='https://plus.google.com/u/0/+Depthlogistics'><img src='http://custom-clearance.depthstaging.com/wp-content/uploads/2018/01/image6.png'></a><a href='https://www.linkedin.com/company/depth-logistics/'><img src='http://custom-clearance.depthstaging.com/wp-content/uploads/2018/01/image7.png'><br><br></a>
".$eol;

//   foreach($muchvar as $exTract ){

//   	$valuee[] = $exTract;
// }   

//   echo "<pre>";
//   var_dump($valuee[1]);
//   echo "</pre><br>";     


$numb = 0;
foreach($muchvar as $tagg){

//	$valuee[] = $tagg;
  if($tagg){
//$file = $path.'/'.$tagg;

//print_r($file);
//$pdfdoc is PDF generated by FPDF
$content = file_get_contents($tagg);
$attachment = chunk_split(base64_encode($content));


// main header
$headers  = "From: ".$from.$eol;
$headers .= "MIME-Version: 1.0".$eol; 
$headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol;

// no more headers after this, we start the body! //

$body .= "Content-Transfer-Encoding: 7bit".$eol;
$body .= "This is a MIME encoded message.".$eol;

// attachment
$body .= "--".$separator.$eol;
$body .= "Content-Type: application/octet-stream; name=\"".$tagg."\"".$eol; 
$body .= "Content-Transfer-Encoding: base64".$eol;
//$body .= "Content-Disposition: attachment; filename=\"".$filename."\"".$eol.$eol;
$body .= "Content-Disposition: attachment".$eol.$eol;
$body .= $attachment.$eol;
$body .= "--".$separator."--";

            }//if 
            else{

              $headers  = "From: ".$from.$eol;
              $headers .= "MIME-Version: 1.0".$eol; 
              $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol;
              // $body .= "--".$separator.$eol;
              // $body .= "--".$separator."--";

            }
    // print_r($tagg).'<br>';
 //    $numb++;
//echo $numb;
     } //for

    //  print_r($valuee[0]);
  
// send message
if (mail($to, $subject, $body, $headers)) {
   // echo "mail send ... OK";
} else {
   // echo "mail send ... ERROR";
}

   
//
}
if( $subject4 !== false ){




 $recentt = $wpdb->get_results("SELECT * FROM dl_dragndrop_f WHERE session_data=$mysession order by data,attach1,attach2,attach3,attach4,attach5,attach6,attach7,attach8,attach9 ");




   			$muchvar = array();
           foreach( $recentt as $rec ){

           	$nfile  = $rec->data;
           	$nfile1 = $rec->attach1;
           	$nfile2 = $rec->attach2;
           	$nfile3 = $rec->attach3;
           	$nfile4 = $rec->attach4;
           	$nfile5 = $rec->attach5;
           	$nfile6 = $rec->attach6;
           	$nfile7 = $rec->attach7;
           	$nfile8 = $rec->attach8;
           	$nfile9 = $rec->attach9;

      	    $at_name = $rec->attach_name;
           	$at_name1 = $rec->attach_name1;
           	$at_name2 = $rec->attach_name2;
           	$at_name3 = $rec->attach_name3;
           	$at_name4 = $rec->attach_name4;
           	$at_name5 = $rec->attach_name5;
           	$at_name6 = $rec->attach_name6;
           	$at_name7 = $rec->attach_name7;
           	$at_name8 = $rec->attach_name8;
           	$at_name9 = $rec->attach_name9;

            }


  $muchvar1  = array($nfile,$nfile1,$nfile2,$nfile3,$nfile4,$nfile5,$nfile6,$nfile7,$nfile8,$nfile9);   
// echo "<pre>";
//    var_dump($muchvar);
//    echo "</pre>";
//    die();




$at_name = $at_name.' | ';
$at_name1 = $at_name1.' | ';
$at_name2 = $at_name2.' | ';
$at_name3 = $at_name3.' | ';
$at_name4 = $at_name4.' | ';
$at_name5 = $at_name5.' | ';
$at_name6 = $at_name6.' | ';
$at_name7 = $at_name7.' | ';
$at_name8 = $at_name8.' | ';
$at_name9 = $at_name9.' | ';



//$filename = "http://custom-clearance.depthstaging.com/wp-content/uploads/cfdb7_uploads/";
$from = "Depth Logistics - Phone 1DEPTH (13 37 84)<enquieriess@depthlogistics.com>";
//$subject = "Test Attachment Email";

$separator = md5(time());

// carriage return type (we use a PHP end of line constant)
$eol = PHP_EOL;

//$path = WP_CONTENT_DIR . '/uploads/file_to_attach.zip';
// attachment name
//$filename = $var;
//foreach( )
$path = 'https://depthlogistics.com/wp-content/uploads/cfdb7_uploads';
// $file = $path . "/" . $filename;


$fillMee = array();

$multiATT = array($at_name,$at_name1,$at_name2,$at_name3,$at_name4,$at_name5,$at_name6,$at_name7,$at_name8,$at_name9);

foreach( $multiATT as $singleATT ){
	
	$fillMee[] = $singleATT;
}
// //$pdfdoc is PDF generated by FPDF
// $content = file_get_contents($file);
// $attachment = chunk_split(base64_encode($content));

// // main header
// $headers  = "From: ".$from.$eol;
// $headers .= "MIME-Version: 1.0".$eol; 
// $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol;

// // no more headers after this, we start the body! //


// $body .= "Content-Transfer-Encoding: 7bit".$eol;
// $body .= "This is a MIME encoded message.".$eol;

// message
$body .= "--".$separator.$eol;
$body .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
$body .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
$body .= $message.$eol;

          $body .=  "<style type='text/css'>
 @font-face {font-family: 'Optima Regular';
  src: url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.eot'); /* IE9*/
  src: url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
  url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.woff2') format('woff2'), /* chrome?firefox */
  url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.woff') format('woff'), /* chrome?firefox */
  url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.ttf') format('truetype'), /* chrome?firefox?opera?Safari, Android, iOS 4.2+*/
  url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.svg#Optima Regular') format('svg'); /* iOS 4.1- */
}
  body{
      font-family: 'Optima Regular';
      /*font-size:medium;*/
  }
  p{
      font-family: 'Optima Regular'
    /*font-size:medium;   */
  }

       span.wpcf7-list-item-label{ font-size: inherit !important; }

    /* Form Number */
   .form-number{background-color:black;color:white;border-radius: 50%; padding-left:7px; padding-right:5px;/*font-size:medium;*/ font-family: 'Optima Regular'; }
   
    /* General */
  input[type='radio'] { display: inline; float: left; width: 18px;font-size:medium; }   
    input[type='text'] { width:100%; }  
  input[type='submit']{ /*background-color:rgb(51,122,183);*/color:white;border-color:1px solid rgb(46, 109, 164);font-size:medium; font-family: 'Optima Regular';}       
    .wpcf7-textarea { width:50% !important; height:100px;font-family: 'Optima Regular';}    
    .wpcf7-text { width:100% !important;  font-family: 'Optima Regular'; }
  /* Submit button */
  #submit-btn{
    /*background-color:rgb(51,122,183);*/
                background-color: #333;
    color:white;
    border-color:1px solid rgb(46, 109, 164);
    font-size:medium; font-family: 'Optima Regular';
  }
  .submit-btn:hover{
    background-color:rgb(40,96,144);            
    color:white;
    border-color:1px solid rgb(32, 77, 116);    
    font-size:medium; font-family: 'Optima Regular';    
  }       
  #chkAgreement{display:block; -webkit-appearance: checkbox; font-size:medium; font-family: 'Optima Regular';}
    /* Proposer */
  #f1-txtName { width:100%; /*font-size:medium;*/ font-family: 'Optima Regular';}
  #f1-txtAddress{ width:70%; /*font-size:medium;*/font-family: 'Optima Regular';}
  #f1-txtPostcode{ width:30%; /*font-size:medium;*/ font-family: 'Optima Regular';}
</style>
<span style='font-size: 16px;font-weight: 700; font-family: Optima;'>Dear $email,</span> <br><br>

<span style='font-size: 16px; font-family: Optima; font-size: 15px;'>Thank you for choosing Depth Logistics. </span>

<p style='padding: 0px 0px;line-height: 22px;width: 750px; font-family: Optima; font-size: 15px;'>We have begun processing the customs clearance of your shipment and we will be back in touch very soon.</p>

<p style='padding: 0px 0px;line-height: 22px;width: 750px; font-family: Optima; font-size: 15px;'>Should you need to contact us we are available 24/7 by <a href='mailto:enquiries@depthlogistics.com'>email</a> and by toll free phone on 1DEPTH (133784). Please quote the tracking number contained in this message to make serving your request more efficient.</p>

<p style='padding: 0px 0px;line-height: 22px;width: 750px; font-family: Optima; font-size: 15px;'>Learn more about how long processing takes and duties and taxes are calculated <a href='https://depthlogistics.com/capabilities/customs-clearance/'>here</a>.</p>

<p style='line-height: 22px;width: 752px; font-family: Optima; font-size: 15px;'>Depth Logistics offers a broad range of other capabilities beyond customs clearance. If you require support with freight forwarding, quarantine and biosecurity, or have any other  logistics enquiry, please click <A href='https://depthlogistics.com/capabilities/'>here</a> to learn more.</p>  

<p style='line-height: 22px;width: 752px; font-family: Optima; font-size: 15px;'>Please find below a summary of the documents and instructions you have provided us. Should we or Customs and Quarantine have any queries we will contact you immediately.</p>

<p style='line-height: 22px;width: 752px; font-family: Optima; font-size: 15px;'>Should we or Customs and Quarantine have any queries we will contact you immediately.</p>

<br>
<table width='750' height='250' border='0' style='background-color:#dbe0e6;' >
<tr><td colspan='2' style='background: #1B273D; font-family: Optima;color: #fff;font-size: 25px;padding: 5px 5px;'>Details: <span style='padding-left: 70px; font-family: Optima;'>Online Export Declaration</span></td></tr>
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>Tracking Number:</th></td> <td style='padding: 8px 7px;font-family: Optima;font-size: 18px; font-weight:bold;'> $tracknumber </td></tr> 

<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>Email address:</td> <td style='background: #fff; padding: 8px 7px;font-family: Optima;font-size: 15px;'> $email </td></tr> 
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>Vessel and Voyager Number:</th></td> <td style='padding: 8px 7px;font-family: Optima;font-size: 15px;'> $vessel_no </td></tr> 
<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>Departure Date:</td> <td style='background: #fff; padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> $depart_date </td></tr> 
<!--<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>ABN:</td> <td style=' padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> [tel-942 ]</td></tr> 
<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>Street Address:</td> <td style='background: #fff; padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> [text-243]</td></tr> 
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>Phone:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> [tel-864]</td></tr> 
<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>Email:</td> <td style='background: #fff; padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> [email-925]
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>Other notes:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> [textarea-860]</td></tr> -->

<!--<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>Tracking number:</td> <td style='background: #fff; padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> [trackernum]</td></tr>--> 

<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>Export Invoice Copy: </td> <td style='padding: 8px 7px; border-left: 0px solid black;font-family: Optima;font-size: 15px;'> 
$fillMee[0] $fillMee[1] $fillMee[2]  </td></tr> 

<!--<tr><td style='font-weight:bold;  width: 160px;font-family: Optima;font-size: 15px;'>Airwaybill:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> [file-819]</td></tr> 
<tr><td style='font-weight:bold; width: 160px; padding: 8px 0px;font-family: Optima;font-size: 15px;background: #fff;'>Marine Insurance Certificate:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;background: #fff;'> [file-368]</td></tr>
<tr><td style='font-weight:bold;padding: 8px 0px; width: 160px;font-family: Optima;font-size: 15px;'>Packing List:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> [file-478]</td></tr>
<tr><td style='font-weight:bold; width: 160px;    padding: 8px 0px;font-family: Optima;font-size: 15px;background: #fff;'>Certificate of Origin:</td> <td style='padding: 8px 7px; border-left: 0px solid black;font-family: Optima;font-size: 15px;background: #fff;'> [file-175]</td></tr>
<tr><td style='font-weight:bold; width: 160px;    padding: 8px 0px;font-family: Optima;font-size: 15px;'> Supplier’s Commercial Invoice:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> [file-532]</td></tr>
<tr><td style='font-weight:bold;  width: 160px;background: #fff;padding: 8px 0px;font-family: Optima;font-size: 15px;'>Photo Identification:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;background: #fff;'> [file-419] </td></tr>
<tr><td style='font-weight:bold;  width: 160px;    padding: 8px 0px;font-family: Optima;font-size: 15px;'>Quarantine Treatment Certificates:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> [file-392]</td></tr>
<tr><td style='font-weight:bold;  width: 160px; background: #fff;   padding: 8px 0px;font-family: Optima;font-size: 15px;'>Other Documents:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;background: #fff;'> [file-411]</td></tr>
<tr><td style='background: #fff; border:2px solid #fff; display:none;'><img src='https://depthlogistics.com/wp-content/uploads/2018/01/depth-logistics.png'></td><td style='background-color: #fff;
    text-align: left;
    padding-left: 10px;
    font-size: 16px;
   display:none;
    font-family: Optima;'>Toll Free: 1DEPTH (13 37 84)<br>
Email: <a href='mailto:enquiries@depthlogistics.com'>enquiries@depthlogistics.com</a><br>
<a href='https://depthlogistics.com/about-us/trading-terms/'>Terms and Conditions</a></td></tr>-->
</table><br><br>

Kind Regards, <br>

The Team at Depth Logistics<br>
<img src='https://depthlogistics.com/wp-content/uploads/2018/01/depth-logistics.png' style='width: 13%;'><br>
Toll Free: 1DEPTH (13 37 84)<br>
Email: <a href='mailto:enquiries@depthlogistics.com'>enquiries@depthlogistics.com</a><br>
<a href='https://depthlogistics.com/about-us/trading-terms/'>Terms and Conditions</a>
<a href='https://plus.google.com/u/0/+Depthlogistics'><img src='http://custom-clearance.depthstaging.com/wp-content/uploads/2018/01/image6.png'></a><a href='https://www.linkedin.com/company/depth-logistics/'><img src='http://custom-clearance.depthstaging.com/wp-content/uploads/2018/01/image7.png'><br><br></a>
".$eol;




foreach( $muchvar1 as $filename ){
  if($filename){
//$file = $path . "/" . $filename;


//$pdfdoc is PDF generated by FPDF
$content = file_get_contents($filename);
$attachment = chunk_split(base64_encode($content));

// main header
$headers  = "From: ".$from.$eol;
$headers .= "MIME-Version: 1.0".$eol; 
$headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol;

// no more headers after this, we start the body! //
$body .= "Content-Transfer-Encoding: 7bit".$eol;
$body .= "This is a MIME encoded message.".$eol;


// attachment
$body .= "--".$separator.$eol;
$body .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol; 
$body .= "Content-Transfer-Encoding: base64".$eol;
//$body .= "Content-Disposition: attachment; filename=\"".$filename."\"".$eol.$eol;
$body .= "Content-Disposition: attachment".$eol.$eol;
$body .= $attachment.$eol;
$body .= "--".$separator."--";

  }// if
    else{

              $headers  = "From: ".$from.$eol;
              $headers .= "MIME-Version: 1.0".$eol; 
              $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol;
              // $body .= "--".$separator.$eol;
              // $body .= "--".$separator."--";

            }
} // for loop
// send message
if (mail($to, $subject, $body, $headers)) {
   // echo "mail send ... OK";
} else {
   // echo "mail send ... ERROR";
}

}
if( $subject5 !== false ){

   $now = $wpdb->get_results("SELECT * FROM dl_db7_forms WHERE session_data=$mysession");

           // $itmes = array();  
           foreach( $now as $ever ){
             $vall = $ever->form_value;
            $array = unserialize($vall);
            //$vall = unserialize($val1);
            //echo "<pre>";
            //print_r($vall);
            //echo "</pre>";
      $var = $array['file-297cfdb7_file'];
      $var1 = $array['file-333cfdb7_file'];
      $var2 = $array['file-368cfdb7_file'];
      // $var3 = $array['file-819cfdb7_file'];
      $var3 = $array['file-898cfdb7_file'];
      $var4 = $array['file-532cfdb7_file'];
      //$var5 = $array['multifile-908cfdb7_file'];
 
           }

$listVar2 = array($var,$var1,$var2,$var3,$var4);
//$filename = "http://custom-clearance.depthstaging.com/wp-content/uploads/cfdb7_uploads/";
$from = "Depth Logistics - Phone 1DEPTH (13 37 84)<enquieriess@depthlogistics.com>";
//$subject = "Test Attachment Email";

$separator = md5(time());

// carriage return type (we use a PHP end of line constant)
$eol = PHP_EOL;

//$path = WP_CONTENT_DIR . '/uploads/file_to_attach.zip';
// attachment name
//$filename = $var;
//foreach( )
$path = 'https://depthlogistics.com/wp-content/uploads/cfdb7_uploads';

// message
$body .= "--".$separator.$eol;
$body .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
$body .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
$body .= $message.$eol;




          $body .=  "<style type='text/css'>
 @font-face {font-family: 'Optima Regular';
  src: url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.eot'); /* IE9*/
  src: url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
  url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.woff2') format('woff2'), /* chrome?firefox */
  url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.woff') format('woff'), /* chrome?firefox */
  url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.ttf') format('truetype'), /* chrome?firefox?opera?Safari, Android, iOS 4.2+*/
  url('/Optima/479fa0cf81d93d6b8ed1797a409a3761.svg#Optima Regular') format('svg'); /* iOS 4.1- */
}
  body{
      font-family: 'Optima Regular';
      /*font-size:medium;*/
  }
  p{
      font-family: 'Optima Regular'
    /*font-size:medium;   */
  }

       span.wpcf7-list-item-label{ font-size: inherit !important; }

    /* Form Number */
   .form-number{background-color:black;color:white;border-radius: 50%; padding-left:7px; padding-right:5px;/*font-size:medium;*/ font-family: 'Optima Regular'; }
   
    /* General */
  input[type='radio'] { display: inline; float: left; width: 18px;font-size:medium; }   
    input[type='text'] { width:100%; }  
  input[type='submit']{ /*background-color:rgb(51,122,183);*/color:white;border-color:1px solid rgb(46, 109, 164);font-size:medium; font-family: 'Optima Regular';}       
    .wpcf7-textarea { width:50% !important; height:100px;font-family: 'Optima Regular';}    
    .wpcf7-text { width:100% !important;  font-family: 'Optima Regular'; }
  /* Submit button */
  #submit-btn{
    /*background-color:rgb(51,122,183);*/
                background-color: #333;
    color:white;
    border-color:1px solid rgb(46, 109, 164);
    font-size:medium; font-family: 'Optima Regular';
  }
  .submit-btn:hover{
    background-color:rgb(40,96,144);            
    color:white;
    border-color:1px solid rgb(32, 77, 116);    
    font-size:medium; font-family: 'Optima Regular';    
  }       
  #chkAgreement{display:block; -webkit-appearance: checkbox; font-size:medium; font-family: 'Optima Regular';}
    /* Proposer */
  #f1-txtName { width:100%; /*font-size:medium;*/ font-family: 'Optima Regular';}
  #f1-txtAddress{ width:70%; /*font-size:medium;*/font-family: 'Optima Regular';}
  #f1-txtPostcode{ width:30%; /*font-size:medium;*/ font-family: 'Optima Regular';}
</style>
<span style='font-size: 16px;font-weight: 700; font-family: Optima;'>Dear $name,</span> <br><br>

<span style='font-size: 16px; font-family: Optima; font-size: 15px;'>Thank you for choosing Depth Logistics. </span>

<p style='padding: 0px 0px;line-height: 22px;width: 750px; font-family: Optima; font-size: 15px;'>We have begun processing the customs clearance of your shipment and we will be back in touch very soon.</p>

<p style='padding: 0px 0px;line-height: 22px;width: 750px; font-family: Optima; font-size: 15px;'>Should you need to contact us we are available 24/7 by <a href='mailto:enquiries@depthlogistics.com'>email</a> and by toll free phone on 1DEPTH (133784). Please quote the tracking number contained in this message to make serving your request more efficient.</p>

<p style='padding: 0px 0px;line-height: 22px;width: 750px; font-family: Optima; font-size: 15px;'>Learn more about how long processing takes and duties and taxes are calculated <a href='https://depthlogistics.com/capabilities/customs-clearance/'>here</a>.</p>

<p style='line-height: 22px;width: 752px; font-family: Optima; font-size: 15px;'>Depth Logistics offers a broad range of other capabilities beyond customs clearance. If you require support with freight forwarding, quarantine and biosecurity, or have any other  logistics enquiry, please click <A href='https://depthlogistics.com/capabilities/'>here</a> to learn more.</p>  

<p style='line-height: 22px;width: 752px; font-family: Optima; font-size: 15px;'>Please find below a summary of the documents and instructions you have provided us. Should we or Customs and Quarantine have any queries we will contact you immediately.</p>

<p style='line-height: 22px;width: 752px; font-family: Optima; font-size: 15px;'>Should we or Customs and Quarantine have any queries we will contact you immediately.</p>

<br>
<table width='750' height='250' border='0' style='background-color:#dbe0e6;' >
<tr><td colspan='2' style='background: #1B273D; font-family: Optima;color: #fff;font-size: 25px;padding: 5px 5px;'>Details: <span style='padding-left: 70px; font-family: Optima;'>Online Personal Effects Customs Clearance</span></td></tr>
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>Tracking Number:</th></td> <td style='padding: 8px 7px;font-family: Optima;font-size: 18px; font-weight:bold;'> $tracknumber </td></tr> 

<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>Email address:</td> <td style='background: #fff; padding: 8px 7px;font-family: Optima;font-size: 15px;'> $email </td></tr> 
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'> First Name </th></td> <td style='padding: 8px 7px;font-family: Optima;font-size: 15px;'> $name </td></tr> 
<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>Family Name:</td> <td style='background: #fff; padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> 
$family_name </td></tr> 
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>Street Address:</td> <td style=' padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> $street_add </td></tr> 
<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'><!--Street Address:--></td> <td style='background: #fff; padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> 
<!-- $street_add --></td></tr> 
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>Phone:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> $phone </td></tr> 
<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>unaccompanied personal effects statement:</td> <td style='background: #fff; padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> <a href='https://depthlogistics.com/b534/?formNo=$session_no'> 
<img src='http://custom-clearance.depthstaging.com/wp-content/uploads/2018/02/36974207.png' style='width:100px; height:100px;' ><br>B534 Form</a><input type='hidden' name='session_no' value='$session_no'></td></tr> 
<tr><td style='font-weight:bold; width: 160px;font-family: Optima;font-size: 15px;'>Bill of Lading or Airwaybill:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> $var </td></tr> 

<tr><td style='font-weight:bold; background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>Photo Identification:</td> <td style='background: #fff; padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> $var1 </td></tr>

<tr><td style='font-weight:bold; width: 160px; font-family: Optima;font-size: 15px;'>Passport Photo Page:</td> <td style='padding: 8px 7px; border-left: 0px solid black;font-family: Optima;font-size: 15px; '> $var2 </td></tr> 

<tr><td style='font-weight:bold;  width: 160px; background: #fff; font-family: Optima;font-size: 15px;'>Packing List:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px; background: #fff;'> $var3 </td></tr> 
<tr><td style='font-weight:bold; width: 160px; padding: 8px 0px;font-family: Optima;font-size: 15px;'> Other Documents:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> $var4 </td></tr> 
  <tr><td style='font-weight:bold;padding: 8px 0px;  background: #fff; width: 160px;font-family: Optima;font-size: 15px;'>Internal Packing List Template </td>  <td style='padding: 8px 7px;border-left: 0px solid black; background: #fff; font-family: Optima;font-size: 15px;'> 
  <a href='https://depthlogistics.com/packing-list-template/?plt=$session_no'> Personal Effects Check list and Quote Basis </a></td></tr>
 <!--<tr><td style='font-weight:bold; width: 160px;    padding: 8px 0px;font-family: Optima;font-size: 15px;background: #fff;'>Certificate of Origin:</td> <td style='padding: 8px 7px; border-left: 0px solid black;font-family: Optima;font-size: 15px;background: #fff;'> [file-175]</td></tr>
<tr><td style='font-weight:bold; width: 160px;    padding: 8px 0px;font-family: Optima;font-size: 15px;'> Supplier’s Commercial Invoice:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> [file-532]</td></tr>
<tr><td style='font-weight:bold;  width: 160px;background: #fff;padding: 8px 0px;font-family: Optima;font-size: 15px;'>Photo Identification:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;background: #fff;'> [file-419] </td></tr>
<tr><td style='font-weight:bold;  width: 160px;    padding: 8px 0px;font-family: Optima;font-size: 15px;'>Quarantine Treatment Certificates:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;'> [file-392]</td></tr>
<tr><td style='font-weight:bold;  width: 160px; background: #fff;   padding: 8px 0px;font-family: Optima;font-size: 15px;'>Other Documents:</td> <td style='padding: 8px 7px;border-left: 0px solid black;font-family: Optima;font-size: 15px;background: #fff;'> [file-411]</td></tr>
<tr><td style='background: #fff; border:2px solid #fff; display:none;'><img src='https://depthlogistics.com/wp-content/uploads/2018/01/depth-logistics.png'></td><td style='background-color: #fff;
    text-align: left;
    padding-left: 10px;
    font-size: 16px;
   display:none;
    font-family: Optima;'>Toll Free: 1DEPTH (13 37 84)<br>
Email: <a href='mailto:enquiries@depthlogistics.com'>enquiries@depthlogistics.com</a><br>
<a href='https://depthlogistics.com/about-us/trading-terms/'>Terms and Conditions</a></td></tr>-->
</table><br><br>

Kind Regards,<br> 

The Team at Depth Logistics<br>
<img src='https://depthlogistics.com/wp-content/uploads/2018/01/depth-logistics.png' style='width: 13%;'><br>
Toll Free: 1DEPTH (13 37 84)<br>
Email: <a href='mailto:enquiries@depthlogistics.com'>enquiries@depthlogistics.com</a><br>
<a href='https://depthlogistics.com/about-us/trading-terms/'>Terms and Conditions</a>
<a href='https://plus.google.com/u/0/+Depthlogistics'><img src='http://custom-clearance.depthstaging.com/wp-content/uploads/2018/01/image6.png'></a><a href='https://www.linkedin.com/company/depth-logistics/'><img src='http://custom-clearance.depthstaging.com/wp-content/uploads/2018/01/image7.png'><br><br></a>

".$eol;



foreach( $listVar2 as $filename ){
  if($filename){
$file = $path . "/" . $filename;


//$pdfdoc is PDF generated by FPDF
$content = file_get_contents($file);
$attachment = chunk_split(base64_encode($content));

// main header
$headers  = "From: ".$from.$eol;
$headers .= "MIME-Version: 1.0".$eol; 
$headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol;

// no more headers after this, we start the body! //
 $body .= "Content-Transfer-Encoding: 7bit".$eol;
 $body .= "This is a MIME encoded message.".$eol;

// attachment
$body .= "--".$separator.$eol;
$body .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol; 
$body .= "Content-Transfer-Encoding: base64".$eol;
//$body .= "Content-Disposition: attachment; filename=\"".$filename."\"".$eol.$eol;
$body .= "Content-Disposition: attachment".$eol.$eol;
$body .= $attachment.$eol;
$body .= "--".$separator."--";

    }//if closure
    else{

              $headers  = "From: ".$from.$eol;
              $headers .= "MIME-Version: 1.0".$eol; 
              $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol;
              // $body .= "--".$separator.$eol;
              // $body .= "--".$separator."--";

            }
            //print_r($file);

    }//for loop



// send message
if (mail($to, $subject, $body, $headers)) {
   // echo "mail send ... OK";
} else {
   // echo "mail send ... ERROR";
}

// print_r($file);
// die();
}


/* Here is   */
     
      return  '<p>Thank you for choosing Depth Logistics. Our team is now processing your customs clearance and will be in touch very soon. Your tracking number is '.$tracknumber. '.<span style="display:none;">'.$send_email.'</span><div id="payUrl"></div></p>';

    }

      
  

  }
  else {

   /* Sending Cancel Payment*/
   // $myChecker = $wpdb->get_results( "SELECT * FROM dl_tps_forms_new WHERE session_data = $mysession AND status = 'pending' limit 1" );
   // foreach( $myChecker as $keyValue ){
   //  $email = $keyValue->email;
   //  $payment_url = $keyValue->payment_url;
   // }
   // if($myChecker){
   //  mail($email, " Pending Transaction ", "Please continue your payment <a href='$payment_url'>here</a> to complete your transaction. " );
   // }
    return '<p style="font-weight:300;">Thank you for choosing Depth Logistics. Please check our <a href="https://depthlogistics.com/capabilities/customs-clearance/">customs clearance.</a></p><div id="payUrl"></div>';

  }


      // Closing for completed payment
        }
      else{
  //        $formID= "EDN2501201807";
  //   $EA = "EDN";
  //   $subject1 = strpos($formID, $EA);
  //   if($subject1 !== false):
  // $print = "EDN";
  //     endif;
// $rr = $wpdb->get_results('SELECT * FROM do_wpcf7pdf_files order by wpcf7pdf_files limit 1');
// foreach( $rr as $tt ){
// $neww = $tt->wpcf7pdf_files;
// }
 //      	$yearNow = date('Y');
	// 	$monNow = date('m');
 // $lastF = $yearNow.'/'.$monNow;
        return '<p style="font-weight:300;">
        Thank you for choosing Depth Logistics. <br>
        We have an enviable track record of “Absolute Reliability” in delivering our clients cargo on time, on budget, safely and securely. 
       <!-- Thank you for choosing Depth Logistics. Please check our <a href="https://depthlogistics.com/capabilities/customs-clearance/"> customs clearance.--></a></p>
        <div></div>';
        //mail("mikky@dephtindustries.com", "Pending Payment", "Please finished payment!");
      }
}
add_shortcode( 'tracker_number', 'foobar_func' );













//shortcode in CF7
add_filter( 'wpcf7_form_elements', 'do_shortcode' );
add_filter( 'wpcf7_skip_mail', function( $skip_mail, $contact_form ) {

global $wpdb;
$wp_session = WP_Session::get_instance();
$mysession = $wp_session['user_name'];

if ( $contact_form->id =='76356' ){
$skip_mail = true;
}

elseif( $contact_form->id == '76360' ){
  $skip_mail = true;
}

elseif( $contact_form->id == '76357'){
  $skip_mail = true;
}

elseif( $contact_form->id == '76359' ){
  $skip_mail = true;
}

elseif( $contact_form->id == '76358' ){
  $skip_mail = true;
}

return $skip_mail;
}, 10, 2 );

/* closing */










add_shortcode('packing_list_template', 'p_list_template');

function p_list_template(){

$random_number = intval( "0" .rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9)  . rand(0,9)  . rand(0,9)  . rand(0,9) );
$wp_session = WP_Session::get_instance();
$wp_session['user_name'] = $random_number;
$mysession =  $wp_session['user_name'];

// $wp_session = WP_Session::get_instance();
// $mysession = $wp_session['user_name'];

$carryMe = "<form id='plT'>
  <table class='table' cellpadding='5'>
    <tbody id='inputboxes'>
       <tr style='background-color: #333; background-color: #333; color:#fff;'>
       <td>
       
      <th> <b>Contents </b></th>
      <th> <b>Value $ Estimate </b></th>
      <th> <b>Wood (Y/N)</b> </th>
      <th> <b>New (Y/N)</b> </th>
      <th> <b>Notes</b> (<em style='font-size:10px;'>if the goood is owned for less than 12 months, please provide date of purchase</em>)  </th>
      <th> <input type='hidden'>&nbsp;</th>
      </td>
      </tr>
      <tr class='light'><td style='position: absolute;margin: 26px -9px;'>1.</td><td><input type='text' name='subject1' required/></td><td><input type='text' name='subject2'/></td><td><input type='text' name='subject3' /></td><td><input type='text' name='subject4' /></td><td><input type='text' name='subject5'first'/></td><td><img id='add' src='https://depthlogistics.com/wp-content/uploads/2018/02/plus-button.png' style='position: absolute;margin:9px 15px;'></td></tr>
    </tbody>
    <tbody>
      <tr class='dark'>
        <td colspan='7'>
          <input type='submit' name='submit' value='Submit' /> <img src='https://depthlogistics.com/wp-content/plugins/contact-form-7/images/ajax-loader.gif' id='imgloading' style='display:none; margin: 15px 11px;'><p id='loadd' style='display:none;'>loading</p>
        </td>
      </tr>
    </tbody>
  </table>
</form>

<span class='error' style='display:none'><div class='wpcf7-form wpcf7-response-output wpcf7-validation-errors'> Please Enter Valid Data </div></span>
<span class='success' style='display:none'><div class='wpcf7-form wpcf7-response-output wpcf7-mail-sent-ok'> Packing List Submitted Success </div></span>
</main>";


return $carryMe;
}



}

add_action( 'wp_ajax_example_ajax_request', 'example_ajax_request' );

// If you wanted to also use the function for non-logged in users (in a theme for example)
 add_action( 'wp_ajax_nopriv_example_ajax_request', 'example_ajax_request' );



add_action('wp_head', 'myplugin_ajaxurl');

function myplugin_ajaxurl() {

   echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
}



remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');



add_shortcode( 'plt_in_page' , 'wp_plt' );
function wp_plt(){

$mysession = $_GET['plt'];
$mysession =	(int)$mysession;
//	$mysession = 34773121171;
 date_default_timezone_set("Australia/Brisbane");
// echo  date("F j, Y  h:i:s A");
global $wpdb;

$extractMe = $wpdb->get_results("SELECT * FROM dl_list_template WHERE session_data='$mysession' limit 1 ");
$detailed = $wpdb->get_results("SELECT * FROM dl_tps_forms_new WHERE session_data='$mysession' limit 1 ");

foreach( $detailed as $detail ){
	$tracker = $detail->tracknumber;
	$form = $detail->form;
}



$tab .= "<div class='container'>";
$tab .= "<div class='row'><br><br><br>";
$tab .= "<div class='col-lg-12 col-md-12'>";
$tab .= "<table class='table' border='1'>";
$tab .= "<tr style='background: grey;color:#fff;'>";
$tab .= "<th><b class='success'>No.</b> </th>";
$tab .= "<th><b class='success'>Contents</b> </th>";
$tab .= "<th><b>Value $ Estimate</b></th>";
$tab .=  "<th> <b>Wood (Y/N)</b> </th>";
$tab .=   "<th> <b>New (Y/N)</b> </th>";
$tab .=   "<th> <b>Notes</b> (<em style='font-size:10px;'>if the goood is owned for less than 12 months, please provide date of purchase</em>)  </th>";
$tab .= "</tr>";

foreach( $extractMe as $valuez ){
 $tent = $valuez->data;

}
parse_str($tent, $output);

  $indexed = array_values($output);


$part = array_chunk($indexed, 5);


// echo "<pre>";
// print_r($part);
// echo "</pre>";


$counMe = 0;
foreach($part as $k => $c) {
    $keys = array_keys($c);
$counMe++;
    $content = $c[$keys[0]];
    $value = $c[$keys[1]];
    $yes = $c[$keys[2]];
    $no = $c[$keys[3]];
    $date = $c[$keys[4]];



$tab .= "<tr>";
$tab .= "<td style=".$colored.">".$counMe."</td>";
$tab .= "<td>". $content ."</td>";
$tab .= "<td>". $value ."</td>";
$tab .= "<td>". $yes ."</td>";
$tab .= "<td>". $no ."</td>";
$tab .= "<td>". $date ."</td>";
$tab .= "</tr>";
}

$tab .= "</table>";
$tab .= "</div>";
$tab .= "</div>";
$tab .= "</div>";
echo $tab;

}


add_action( 'wp_ajax_dragme_request', 'dragme_request' );
add_action( 'wp_ajax_nopriv_dragme_request', 'dragme_request' );

function dragme_request() {

        // $wp_session = WP_Session::get_instance();
        // $mysession = $wp_session['user_name'];

    // The $_REQUEST contains all the data sent via ajax 
  if ( isset($_REQUEST) ) {

     echo $fruit = $_REQUEST['fruit'];


 //$fruit = $_REQUEST['fruit']['name'];
 
		/* Getting file name */
	   	$filename = $_FILES['file']['name'];

		/* Getting File size */
		$filesize = $_FILES['file']['size'];

	
		$location = "$path = 'https://depthlogistics.com/wp-content/uploads/cfdb7_uploads/".$filename;
		//$path = 'https://depthlogistics.com/wp-content/uploads/cfdb7_uploads';

		// if($location){
		// 	echo "<script>alert('did work!');</script>";
		// }
		// else{
		// 	echo "<script>alert('not today');</script>";
		// }

		$return_arr = array();

		/* Upload file */
		if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
		    $src = "default.png";

		    // checking file is image or not
		    if(is_array(getimagesize($location))){
		        $src = $location;
		    }
		    $return_arr = array("name" => $filename,"size" => $filesize, "src"=> $src);
		    echo "success bithc";
		}
else{
	echo "not today";
}
 json_encode($return_arr);

 


    }
 }

